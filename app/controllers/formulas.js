var express = require('express'); // call express
var router = express.Router(); // get an instance of the express Router
var Formula = require('../models/formula');
var Element = require('../models/element');

router.route('/formula').post(function(req, res) {

  var formula = new Formula();
  formula.name = req.body.name;
  formula.description = req.body.description;
  formula.rating = 0;

  //save formula
  formula.save(function(err) {
    if (err)
      res.send(err);
    res.json({
      message: 'Formula initiated'
    });
  });
});

router.route('/:formula_id/element/').post(function(req, res) {
  var element = new Element();
  element.question = req.body.question;
  var formula = Formula.findById(req.params.formula_id, function(err, formula) {
    if (err)
      res.send(err);
    element.formula = formula;
    element.save(function(err) {
      if (err)
        res.send(err);
      formula.elements.push(element);
      formula.save(function(err) {
        if (err)
          res.send(err);
        res.json({
          message: 'Element added to formula'
        });
      });

    });

  });
});

module.exports = router;
