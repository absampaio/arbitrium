var express = require('express')
  , router = express.Router()

router.use('/api', require('./formulas'));

module.exports = router;
