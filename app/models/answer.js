var mongoose = require('mongoose');

var AnswerSchema = new mongoose.Schema({
  answer: String,
  value: Number,
  element: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Element'
  }
});

module.exports = mongoose.model("Answer", AnswerSchema);
