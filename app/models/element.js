var mongoose = require('mongoose');

var ElementSchema = new mongoose.Schema({
  question: String,
  formula: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Formula'
  },
  answers: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Answer'
  }]
});

module.exports = mongoose.model("Element", ElementSchema);
