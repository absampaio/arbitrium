var mongoose = require('mongoose');

var FormulaSchema = new mongoose.Schema({
  name: String,
  description: String,
  rating: Number,
  elements: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Element'
  }]
});

module.exports = mongoose.model("Formula", FormulaSchema);
